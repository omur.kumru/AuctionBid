package com.kmr.auctionbid;

import com.kmr.auctionbid.utils.ValidationCheck;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class PasswordValidatorTest {

    @Test
    public void password_validator_correct_simple_returns_true() {
        assertTrue(ValidationCheck.isValidPassword("Caps213+"));
    }

    @Test
    public void password_validator_correct_simple_returns_true2() {
        assertTrue(ValidationCheck.isValidPassword("Caps213@"));
    }

    @Test
    public void password_validator_correct_simple_returns_true3() {
        assertTrue(ValidationCheck.isValidPassword("Caps213&"));
    }

    @Test
    public void password_validator_correct_simple_returns_true4() {
        assertTrue(ValidationCheck.isValidPassword("cAps213&"));
    }

    @Test
    public void password_validator_invalid_simple_returns_false() {
        assertFalse(ValidationCheck.isValidPassword("caps"));
    }

    @Test
    public void password_validator_invalid_simple_returns_false2() {
        assertFalse(ValidationCheck.isValidPassword("Caps"));
    }

    @Test
    public void password_validator_invalid_simple_returns_false3() {
        assertFalse(ValidationCheck.isValidPassword("Caps213"));
    }
}
