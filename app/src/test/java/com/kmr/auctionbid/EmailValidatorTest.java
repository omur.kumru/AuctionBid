package com.kmr.auctionbid;

import com.kmr.auctionbid.utils.ValidationCheck;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class EmailValidatorTest {

    @Test
    public void email_validator_correct_email_simple_returns_true() {
        assertTrue(ValidationCheck.isValidEmail("name@email.com"));
    }

    @Test
    public void emailValidator_CorrectEmailSubDomain_ReturnsTrue() {
        assertTrue(ValidationCheck.isValidEmail("name@email.co.uk"));
    }

    @Test
    public void email_validator_invalid_email_simple_returns_false() {
        assertFalse(ValidationCheck.isValidEmail("name@email"));
    }

    @Test
    public void emailValidator_InvalidEmailDoubleDot_ReturnsFalse() {
        assertFalse(ValidationCheck.isValidEmail("name@email..com"));
    }

    @Test
    public void emailValidator_InvalidEmailNoUsername_ReturnsFalse() {
        assertFalse(ValidationCheck.isValidEmail("@email.com"));
    }


    @Test
    public void emailValidator_EmptyString_ReturnsFalse() {
        assertFalse(ValidationCheck.isValidEmail(""));
    }

    @Test
    public void emailValidator_NullEmail_ReturnsFalse() {
        assertFalse(ValidationCheck.isValidEmail(null));
    }
}
