package com.kmr.auctionbid.db;


import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.kmr.auctionbid.entity.Item;
import java.io.Closeable;
import java.io.IOException;

public class DBAdapter implements Closeable {

    private static final String TAG = "DBAdapter";
    private static final String DBNAME = "db";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLENAME = "itemsinfo";
    private  static final String TEXT_TYPE = " TEXT";
    private static final String TABLECREATE = String.format("create table %s (%s integer primary key autoincrement, %s varchar, %s varchar, %s varchar,  %s varchar, %s varchar, %s varchar)", TABLENAME, ItemAdapter.ID, ItemAdapter.NAME, ItemAdapter.PRICE, ItemAdapter.TIME, ItemAdapter.COUNT, ItemAdapter.PATH, ItemAdapter.WON);
    private static final String TABLEDROP = String.format("drop table if exists ");
    private final Context context;
    private DatabaseHelper dbHelper; // O anki adaptörün referansı
    private SQLiteDatabase db;
    private IDBAdapter<Item> itemAdapter;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        dbHelper = new DatabaseHelper(context);
        this.open();
        itemAdapter = new ItemAdapter(db);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        private Context m_context;

        public DatabaseHelper(Context context)
        {
            super(context, DBNAME, null, DATABASE_VERSION);
            m_context = context;
        }


        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(TABLECREATE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            db.execSQL(TABLEDROP + TABLENAME);
            onCreate(db);
        }
    }

    //---opens the database---
    public DBAdapter open() throws SQLException
    {
        db = dbHelper.getWritableDatabase(); // Veritabanı bağlantısını açmak için gereken metot

        return this;
    }

    //---closes the database---
    @Override
    public void close() throws IOException {
        dbHelper.close();
    } // DatabaseHelper and connection are closed together

    public boolean insertItem(Item it)
    {
        return itemAdapter.insert(it);
    }

    public boolean deleteItem(Item it)
    {
        return itemAdapter.delete(it);
    }


    public boolean updateItem(Item it)
    {
        return  itemAdapter.update(it);
    }

    public Cursor getItemsAsCursor()
    {
        return db.query(ItemAdapter.TABLENAME,
                new String[] {ItemAdapter.ID, ItemAdapter.NAME, ItemAdapter.PRICE, ItemAdapter.TIME, ItemAdapter.COUNT, ItemAdapter.PATH, ItemAdapter.WON},
                null, null, null, null, null);
    }

    public Cursor getItem(int id) throws SQLException
    {
        return db.query(true, ItemAdapter.TABLENAME, new String[] {ItemAdapter.ID, ItemAdapter.NAME, ItemAdapter.PRICE, ItemAdapter.TIME, ItemAdapter.COUNT, ItemAdapter.PATH, ItemAdapter.WON}, ItemAdapter.ID + "=" + id, null, null, null, null, null);
    }
}
