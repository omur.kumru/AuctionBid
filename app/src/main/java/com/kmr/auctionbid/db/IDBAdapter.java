package com.kmr.auctionbid.db;

import java.util.List;


public interface IDBAdapter<T> {
    boolean insert(T t);
    boolean delete(T t);
    boolean update(T t);
    List<T> getAll();
}
