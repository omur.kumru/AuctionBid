package com.kmr.auctionbid.db;

import android.content.Context;

import com.kmr.auctionbid.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Util {

    public static boolean isDBExists(Context context, String dbName)
    {
        File file = context.getDatabasePath(dbName);

        return file.exists();
    }

    public static boolean copyDbIfNotExists(Context context, String dbName)
    {
        if (isDBExists(context, dbName))
            return false;

        boolean status = false;

        try {
            InputStream src = context.getResources().openRawResource(R.raw.db);
            File dest = context.getDatabasePath(dbName);

            status = copyFile(src, dest, 1024);
        }
        catch (Exception ex) {

        }

        return status;

    }

    public static boolean copyFile(InputStream fis, File dest, int blockSize)
    {
        boolean status = true;

        try (FileOutputStream fos = new FileOutputStream(dest)) {
            byte [] buf = new byte[blockSize];

            int read;

            while ((read = fis.read(buf)) > 0)
                fos.write(buf, 0, read);
        }
        catch (Throwable ex) {
            status = false;
        }

        return status;
    }


}
