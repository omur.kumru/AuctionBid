package com.kmr.auctionbid.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.kmr.auctionbid.entity.Item;

import java.util.List;


public class ItemAdapter implements IDBAdapter<Item> {

    private SQLiteDatabase db;
    public static final String ID = "item_id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String TIME = "time";
    public static final String COUNT = "bid_count";
    public static final String PATH = "picture_path";
    public static final String TABLENAME = "itemsinfo";
    public static final String WON = "whoWon";

    public ItemAdapter(SQLiteDatabase db) {
        this.db = db;
    }

    @Override
    public boolean insert(Item item) {

        //insert into people p (p.name, p.phone) values (name, phone)

        ContentValues initialValues = new ContentValues();

        initialValues.put(NAME, item.getItemName());
        initialValues.put(PRICE, item.getItemPrice());
        initialValues.put(TIME, item.getItemTime());
        initialValues.put(COUNT, item.getBidCount());
        initialValues.put(PATH, item.getPicturePath());
        initialValues.put(WON, item.getWhoWonFlag());

        return db.insertOrThrow(TABLENAME, null, initialValues) > 0;
    }

    @Override
    public boolean delete(Item item) {
        return db.delete(TABLENAME, ID + "=" + item.getId(), null) > 0;
    }

    @Override
    public boolean update(Item item) {
        ContentValues cv = new ContentValues();

        cv.put(ID, item.getId());
        cv.put(NAME, item.getItemName());
        cv.put(PRICE, item.getItemPrice());
        cv.put(TIME, item.getItemTime());
        cv.put(COUNT, item.getBidCount());
        cv.put(PATH, item.getPicturePath());
        cv.put(WON, item.getWhoWonFlag());

        return db.update(TABLENAME, cv, ID + "=" + item.getId(), null) > 0;
    }

    //prefer returning empty list values to null from list valued method
    @Override
    public List<Item> getAll() {

        return null;
    }
}
