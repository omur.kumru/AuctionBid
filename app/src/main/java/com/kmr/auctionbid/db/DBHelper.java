package com.kmr.auctionbid.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.widget.Toast;

import com.kmr.auctionbid.entity.Item;

import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    private Context context;  // Sandbox'a erişim için gerekli

    public DBHelper(Context context) {
        this.context = context;
    }

    public boolean insert(Item it)
    {
        boolean status = true;

        try (DBAdapter da = new DBAdapter(context)) {
            status = da.insertItem(it);
        }
        catch (Throwable ex) {
            status = false;
            Toast.makeText(context, ex.getClass() + ":" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return status;
    }

    public boolean update(Item it)
    {
        boolean status = true;

        try (DBAdapter da = new DBAdapter(context)) {
            status = da.updateItem(it);
        }
        catch (Throwable ex) {
            status = false;
        }

        return status;
    }

    public boolean delete(int id)
    {
        boolean status = true;

        try (DBAdapter da = new DBAdapter(context)) {
            status = da.deleteItem(new Item(id, "", "","","","", ""));
        }
        catch (Throwable ex) {
            status = false;
        }

        return status;
    }

    public List<Item> getItems()
    {
        ArrayList<Item> items = new ArrayList<>();

        try (DBAdapter da = new DBAdapter(context)) {
            Cursor c = da.getItemsAsCursor();
            int i=0;
            while (c.moveToNext()){
                items.add(new Item(c.getInt(0), c.getString(1), c.getString(2),c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
                i++;
            }
        }
        catch (Throwable ex) {
            items = null;
        }

        return items;
    }

    public Item getItemByID(int id)
    {
        Item p = null;

        try (DBAdapter da = new DBAdapter(context); Cursor c = da.getItem(id)) {

            p = c.moveToFirst() ? new Item(c.getInt(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)) : null;
        }
        catch (SQLException ex) {
            //...
        }
        catch (Throwable ex) {
            //...
        }

        return p;
    }
}
