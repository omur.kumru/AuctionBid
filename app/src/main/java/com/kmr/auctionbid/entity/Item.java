package com.kmr.auctionbid.entity;

import java.io.Serializable;

public class Item implements Serializable {

    private int id;
    private String itemName, itemTime, itemPrice;
    private String bidCount, picturePath, whoWonFlag;

    public Item(String itemName, String itemPrice, String itemTime, String bidCount, String picturePath, String whoWonFlag) {
        this.itemName = itemName;
        this.itemTime = itemTime;
        this.itemPrice = itemPrice;
        this.bidCount =  bidCount;
        this.picturePath = picturePath;
        this.whoWonFlag = whoWonFlag;

    }

    public Item(int id, String itemName, String itemPrice, String itemTime, String bidCount, String picturePath, String whoWonFlag) {
        this.id = id;
        this.itemName = itemName;
        this.itemTime = itemTime;
        this.itemPrice = itemPrice;
        this.bidCount = bidCount;
        this.picturePath = picturePath;
        this.whoWonFlag = whoWonFlag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemTime() {
        return itemTime;
    }

    public void setItemTime(String itemTime) {
        this.itemTime = itemTime;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getBidCount() {
        return bidCount;
    }

    public void setBidCount(String bidCount) {
        this.bidCount = bidCount;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getWhoWonFlag() {
        return whoWonFlag;
    }

    public void setWhoWonFlag(String whoWonFlag) {
        this.whoWonFlag = whoWonFlag;
    }
}
