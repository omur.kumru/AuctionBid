package com.kmr.auctionbid.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kmr.auctionbid.R;

import static com.kmr.auctionbid.utils.ValidationCheck.isValidEmail;
import static com.kmr.auctionbid.utils.ValidationCheck.isValidPassword;


public class LoginActivity extends AppCompatActivity {

    private EditText emailEdittext;
    private EditText passwordEdittext;
    private Button hesapOlusturButton;
    private Button girisButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        initViews();
        initEvents();

    }

    private void initViews() {
        emailEdittext = (EditText) findViewById(R.id.email_login_edittext);
        passwordEdittext = (EditText) findViewById(R.id.password_login_edittext);
        hesapOlusturButton = (Button) findViewById(R.id.main_button_hesapOlustur);
        girisButton = (Button) findViewById(R.id.main_button_giris);
    }

    private void initEvents() {

        hesapOlusturButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, HesapOlusturActivity.class);
                startActivity(intent);

            }
        });

        girisButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                boolean emailValidCheck = isValidEmail(emailEdittext.getText());
                boolean passwordValidCheck = isValidPassword(passwordEdittext.getText().toString());

                if (emailValidCheck && passwordValidCheck) {

                    SharedPreferences sp1=LoginActivity.this.getSharedPreferences(getString(R.string.login),Context.MODE_PRIVATE);

                    String unm=sp1.getString("Unm", null);
                    String pass = sp1.getString("Psw", null);

                    if(unm.equals(emailEdittext.getText().toString())&&pass.equals(passwordEdittext.getText().toString())){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }else
                        Toast.makeText(LoginActivity.this, R.string.not_equal_password,Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(LoginActivity.this, R.string.not_valid_password, Toast.LENGTH_LONG).show();
            }
        });
    }


}
