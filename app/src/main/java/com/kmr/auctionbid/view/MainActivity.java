package com.kmr.auctionbid.view;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.kmr.auctionbid.CustomAdapter;
import com.kmr.auctionbid.R;
import com.kmr.auctionbid.db.DBHelper;
import com.kmr.auctionbid.db.Util;
import com.kmr.auctionbid.entity.Item;

import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private String newItemName;
    private String newItemPrice;
    private String newItemTime;
    private ListView listView;
    private CustomAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int PICK_IMAGE_REQUEST = 1;
    private Dialog formDialg;
    private Uri selectedImage;
    private String newPicturePath;
    private UserBotThread userBotThread;

    //random auction
    //get random elements
    //change price random
    //need random price and random element

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDB();
        // initRandomList();
        loadItems();
        initToolbar();
        initEvents();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                addSubmitItem();
                break;
            default:
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userBotThread.interrupt();
    }

    private void initDB() {
        try {
            Util.copyDbIfNotExists(this, "db");
        } catch (Throwable ex) {
            Log.d("exxxxx", ex.toString());
        }
    }

    private void loadItems() {
        try {
            DBHelper db = new DBHelper(this);
            List<Item> items = db.getItems();
            if (items == null)
                return;

            listView = (ListView) findViewById(R.id.list_view);
            adapter = new CustomAdapter(MainActivity.this, items);
            listView.setAdapter(adapter);
            /////////check the listview null or not
            if (adapter.getCount() != 0) {
                userBotThread = new UserBotThread();
                userBotThread.start();
            }
            /////////check the listview null or not

        } catch (Throwable ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initToolbar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
    }

    private void initEvents() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String expired = adapter.getItemTime(position);
                if (expired.equals(getString(R.string.expired))) {
                    view.setFocusable(false);
                } else {
                    bidItem(position);
                }
            }
        });

    }

    private void bidItem(final int position) {

        final Dialog bidDialog = new Dialog(MainActivity.this);
        bidDialog.setContentView(R.layout.bid_item);
        Button yesButton = (Button) bidDialog.findViewById(R.id.btn_yes);
        Button noButton = (Button) bidDialog.findViewById(R.id.btn_no);

        final EditText edtxtPrice = (EditText) bidDialog.findViewById(R.id.input_price);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newItemPrice = edtxtPrice.getText().toString();
                String beforeItemPrice = getItemData(position);
                if (validatePrice(newItemPrice, beforeItemPrice)) {

                    adapter.updatePrice(position, newItemPrice);
                    adapter.updateCount(position);
                    adapter.updateWhoWon(position, getString(R.string.user));

                    updateItemPriceData(position + 1, newItemPrice);
                    updateItemBidData(position + 1);
                    updateItemWhoWonData(position + 1, getString(R.string.user));
                    bidDialog.dismiss();
                } else {
                    makeAlertMessage();
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bidDialog.dismiss();
            }
        });
        bidDialog.show();
    }

    private void timer(final int position, int remainingTime) {
        new CountDownTimer(remainingTime * 1000, 1000) {
            int secondsLeft = 0;

            public void onTick(long millisUntilFinished) {
                if (Math.round((float) millisUntilFinished / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) millisUntilFinished / 1000.0f);
                    adapter.updateTime(position, secondsLeft);
                }
            }

            public void onFinish() {
                adapter.updateTimeFinished(position, getString(R.string.expired));  //changed
                updateItemTimeData(position);
                listView.getChildAt(position).setOnClickListener(null);
                this.cancel();
            }
        }.start();
    }

    private void addSubmitItem() {
        formDialg = new Dialog(MainActivity.this);
        formDialg.setContentView(R.layout.add_item);
        Button yesButton = (Button) formDialg.findViewById(R.id.btn_yes);
        Button noButton = (Button) formDialg.findViewById(R.id.btn_no);
        ImageView imgagePreview = (ImageView) formDialg.findViewById(R.id.preview_images);

        final EditText edtxtName = (EditText) formDialg.findViewById(R.id.input_name);
        final EditText edtxtTime = (EditText) formDialg.findViewById(R.id.input_time);
        final EditText edtxtPrice = (EditText) formDialg.findViewById(R.id.input_price);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newItemName = edtxtName.getText().toString();
                newItemPrice = edtxtPrice.getText().toString();
                newItemTime = edtxtTime.getText().toString();
                insertData(newItemName, newItemPrice, newItemTime, newPicturePath);
                int size = getCountBidItems();
                Log.d("sizeee", String.valueOf(size));
                timer(size - 1, Integer.parseInt(newItemTime));
                //adapter.updateData(newItemName, newItemTime, newItemPrice);  //TODO
                formDialg.dismiss();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                formDialg.dismiss();
            }
        });

        imgagePreview.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Explain to the user why we need to read the contacts
                    }

                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                    return;
                }

                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, PICK_IMAGE_REQUEST);
                formDialg.dismiss();
            }
        });
        formDialg.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            newPicturePath = cursor.getString(columnIndex);
            cursor.close();
            formDialg.show();
            ImageView imageView = (ImageView) formDialg.findViewById(R.id.preview_images);
            imageView.setImageBitmap(BitmapFactory.decodeFile(newPicturePath));
        }
    }

    private boolean validatePrice(String price, String beforePrice) {
        int priceVal = Integer.parseInt(price);
        int beforePriceVal = Integer.parseInt(beforePrice);
        if (priceVal <= beforePriceVal) {
            return false;
        }
        return true;
    }

    private void makeAlertMessage() {
        Toast.makeText(MainActivity.this, R.string.price_error_message, Toast.LENGTH_LONG).show();
    }

    private void insertData(String name, String price, String time, String picturePath) {
        DBHelper db = new DBHelper(this);
        Item item = new Item(name, price, time, getString(R.string.start_value), picturePath, getString(R.string.start_value));  //change id //TODO

        if (db.insert(item))
            this.loadItems(); //update listview  //Can change //TODO
        else
            Toast.makeText(this, R.string.error_nessage, Toast.LENGTH_LONG).show();
    }

    //change get data
    private String getItemData(int position) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position + 1);  //0 = 1
        return item.getItemPrice();
    }


    private int getCountBidItems() {
        DBHelper db = new DBHelper(this);
        return db.getItems().size();
    }

    private String getPriceBidItems(int position) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position);
        return item.getItemPrice();
    }

    //Change update data 4 methods
    private void updateItemTimeData(int position) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position + 1);
        item.setItemTime(getString(R.string.expired));
        db.update(item);
    }

    private void updateItemBidData(int position) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position);
        String count = item.getBidCount();
        int countInt = Integer.parseInt(count);
        countInt++;
        count = String.valueOf(countInt);
        item.setBidCount(count);
        db.update(item);
    }

    private void updateItemPriceData(int position, String newItemPrice) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position);
        item.setItemPrice(newItemPrice);
        db.update(item);
    }

    private void updateItemWhoWonData(int position, String flag) {
        DBHelper db = new DBHelper(this);
        Item item = db.getItemByID(position);
        item.setWhoWonFlag(flag);
        db.update(item);
    }

    public int getRandomSize() {
        int size = getCountBidItems();
        Random random = new Random();
        return random.nextInt(size) + 1;
    }

    public int getRandomPrice(int position) {
        Random random = new Random();
        int randPrice = random.nextInt(200) + 1;
        return randPrice + Integer.parseInt(getPriceBidItems(position));
    }

    class UserBotThread extends Thread {
        public void run() {
            Random rand = new Random();
            while(true) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int position = getRandomSize();
                        int randomPrice = getRandomPrice(position);
                        //price update
                        updateItemPriceData(position, String.valueOf(randomPrice));
                        adapter.updatePrice(position - 1, String.valueOf(randomPrice));
                        //bid count
                        updateItemBidData(position);
                        adapter.updateCount(position - 1);
                        //who win the auction
                        updateItemWhoWonData(position, getString(R.string.bot));
                        adapter.updateWhoWon(position-1, getString(R.string.bot));

                        Log.d("USERTHREAD", String.valueOf(randomPrice));
                    }
                });
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
