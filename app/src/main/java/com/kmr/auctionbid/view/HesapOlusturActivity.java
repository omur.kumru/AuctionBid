package com.kmr.auctionbid.view;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.kmr.auctionbid.R;

import static com.kmr.auctionbid.utils.ValidationCheck.isPasswordSame;
import static com.kmr.auctionbid.utils.ValidationCheck.isValidEmail;
import static com.kmr.auctionbid.utils.ValidationCheck.isValidPassword;


public class HesapOlusturActivity extends AppCompatActivity {

    private EditText emailEdittext;
    private EditText passwordEdittext;
    private EditText passwordAgainEdittext;
    private Button hesapOlusturButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hesap_olustur);
        getSupportActionBar().hide();
        initViews();
        initEvents();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void initViews() {

        emailEdittext= (EditText) findViewById(R.id.email_create_edittext);
        passwordEdittext= (EditText) findViewById(R.id.password_create_edittext);
        passwordAgainEdittext= (EditText) findViewById(R.id.passwordagain_create_edittext);
        hesapOlusturButton=(Button)findViewById(R.id.hesapOlustur_hesapolustur_button);

    }

    private void initEvents() {

        hesapOlusturButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                boolean emailValidCheck = isValidEmail(emailEdittext.getText());
                boolean passwordValidCheck = isValidPassword(passwordEdittext.getText().toString());
                boolean passwordSameCheck=isPasswordSame(passwordEdittext.getText().toString(),passwordAgainEdittext.getText().toString());

                if (emailValidCheck && passwordValidCheck&&passwordSameCheck) {

                    SharedPreferences sp=getSharedPreferences(getString(R.string.login), Context.MODE_PRIVATE);
                    SharedPreferences.Editor Ed=sp.edit();
                    Ed.putString("Unm",emailEdittext.getText().toString() );
                    Ed.putString("Psw",passwordEdittext.getText().toString());
                    Ed.commit();

                    Toast.makeText(HesapOlusturActivity.this, R.string.user_created,Toast.LENGTH_LONG).show();
                    //Intent intent = new Intent(HesapOlusturActivity.this, MainActivity.class);

                    // startActivity(intent);
                } else if(passwordSameCheck==false)
                    Toast.makeText(HesapOlusturActivity.this, R.string.not_equal_password, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(HesapOlusturActivity.this, R.string.not_valid_password, Toast.LENGTH_LONG).show();

            }
        });


    }
}
