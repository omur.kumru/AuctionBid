package com.kmr.auctionbid;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kmr.auctionbid.entity.Item;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    private List<Item> items;
    Context context;
    private static LayoutInflater inflater = null;


    public CustomAdapter(Context context, List<Item> itemList) {
        // TODO Auto-generated constructor stub
        this.items = itemList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void updatePrice(int position,String price) {
        items.get(position).setItemPrice(price);
        this.notifyDataSetChanged();
    }

    public void updateTime(int position, int time) {
        items.get(position).setItemTime(time+"");
        this.notifyDataSetChanged();
    }

    public void updateWhoWon(int position, String whoWonFlag) {
        items.get(position).setWhoWonFlag(whoWonFlag);
        this.notifyDataSetChanged();
    }

    public void updateCount(int position) {
        String bidCount = items.get(position).getBidCount();
        int count;
        if (bidCount.equals("")) {
            count = 0;
        }else {
            count = Integer.parseInt(bidCount);
        }
        count++;
        bidCount = String.valueOf(count);
        items.get(position).setBidCount(bidCount);
        this.notifyDataSetChanged();
    }

    public void updateTimeFinished(int position, String text) {
        items.get(position).setItemTime(text);
        this.notifyDataSetChanged();
    }

    public String getItemTime(int position){
        return items.get(position).getItemTime();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView txtName;
        TextView txtTime;
        TextView txtPrice;
        TextView txtBidCount;
        TextView txtWhoWon;
        ImageView imageView;

        public void setData(Item item) {
            txtName.setText(item.getItemName());
            txtPrice.setText(item.getItemPrice());
            txtTime.setText(item.getItemTime());
            txtWhoWon.setText(item.getWhoWonFlag());
            txtBidCount.setText(item.getBidCount());
            imageView.setImageBitmap(BitmapFactory.decodeFile(item.getPicturePath()));
        }

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView = inflater.inflate(R.layout.list_row, null);
        holder.txtName = (TextView) rowView.findViewById(R.id.item_name);
        holder.txtTime = (TextView) rowView.findViewById(R.id.remaining_time);
        holder.txtPrice = (TextView) rowView.findViewById(R.id.item_price);
        holder.txtBidCount = (TextView) rowView.findViewById(R.id.bid_count);
        holder.imageView = (ImageView) rowView.findViewById(R.id.picture);
        holder.txtWhoWon = (TextView) rowView.findViewById(R.id.item_who_won);
        holder.setData(items.get(position));
        return rowView;
    }


}
