package com.kmr.auctionbid;

import android.test.ActivityInstrumentationTestCase;
import android.widget.Button;
import android.widget.EditText;

import com.kmr.auctionbid.view.LoginActivity;




public class LoginActivityTest extends ActivityInstrumentationTestCase<LoginActivity>{


    private LoginActivity mTestActivity;
    private EditText emailEdittext;
    private EditText passwordEdittext;
    private Button hesapOlusturButton;
    private Button girisButton;

    public LoginActivityTest(String pkg, Class<LoginActivity> activityClass) {
        super(pkg, activityClass);
    }


    public void setUp() throws Exception{
        super.setUp();

        mTestActivity = getActivity();
        emailEdittext = (EditText) mTestActivity.findViewById(R.id.email_create_edittext);

    }

    public void testPreConditions() {
        assertNotNull("mTestActivity is null", mTestActivity);
        assertNotNull("emailEdittext is null", emailEdittext);
    }
}
